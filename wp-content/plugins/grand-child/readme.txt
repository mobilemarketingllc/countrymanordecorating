=== Grand Child Theme ===
Contributors: devteam
Tags: product listing, theme custmization
Requires at least: 4.6
Tested up to: 5.0
Stable tag: 1.3.87
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A WordPress Grandchild Theme (as a plugin).

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

A WordPress Grandchild Theme (as a plugin)

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about Grand Child Plugin? =

A WordPress Grandchild Theme (as a plugin)



== Changelog ==
 1.3.87 =

* Removed duplicate entries from 301 redirect.

 1.3.86 =

* Changed Condition for liqifire featured product loop.

 1.3.85 =

* Replaced cloudinary url to liquifire for slider.

 1.3.84 =

* Added cloudinary url to replace liquifire. 

 1.3.83 =

* CSV import and blog sync integrated.

 1.3.82 =

* Added condition for empty shapeand installation in pdp page.

 1.3.81 =

* Added condition for empty gallery image in pdp page.

 1.3.80 =

* Added condition for empty attribute in pdp page.

 1.3.79 =

* Created new shortcode for link in blog post.

 1.3.78 =

* Changed condition for Blog Sync as CDE date.

 1.3.77 =

* Changed condition for Blog Sync

= 1.3.76 =

* Optimized PDP page database query for speed performance.

= 1.3.74 =

* Changed condition for sale form hidden fields.

= 1.3.72 =

* Removed 301 same redirect url from redirection table.

= 1.3.70 =

* Created shortcode for financing which reurns only link.

= 1.3.69 =

* Changed condition for financing shortcode if empty links in CDE

= 1.3.68 =

* Changed condition for financing shortcode and  also changed get coupon on featured product slider

= 1.3.67 =

* Added slash to end of bredcrumbs url.

= 1.3.66 =

* Changed filter for robots txt.

= 1.3.65 =

* Added al tag for modal in post grid.

= 1.3.64 =

* Changed timing for 301 and 404 cron job.

= 1.3.63 =

* Added new rirection plugin logic and changed sync 301 redirect logic.

= 1.3.61 =

* Added css for comments off and changed sync cron job for other brand than shaw,mohawk.

= 1.3.59 =

* Added minified css and js

= 1.3.58 =

* Change condition for seo sitemap url.

= 1.3.57 =

* Change rugshop condition for gravity form hidden field.

= 1.3.56 =

* Added print coupon image functionality.

= 1.3.55 =

* Updated bump request function.

= 1.3.53 =

* Get Financing button text and url change on PLP

= 1.3.52 =

* Changed auto sync timetable for carpet shaw and mohawk brannd

= 1.3.51 =

* Get Financing button text and url change on PDP and PLP.

= 1.3.50 =

* H1 added for choose your color and choose your style.

= 1.3.48 =

* Changed fields for block country and added ip for whitelist.

= 1.3.47 =

* changes sale form hidden fields as cde response changed.

= 1.3.46 =

* Updated time for custom redirect cron.

= 1.3.44 =

* Updated block country options.

= 1.3.42 =

* Updated block country options.

= 1.3.41 =

* Updated cron timing for catalog.

= 1.3.40 =

* Updated wordpress timezone to UTC-5.

= 1.3.39 =

* Changed accessible code due to changed response in cde

= 1.3.38 =

* Changed interval time for laminate

= 1.3.37 =

* Changed interval time for laminate

= 1.3.36 =

* Clodinary and Daltile condition changed

= 1.3.34 =

* 301 url slashes added.

= 1.3.33 =

* Post Grid gallery image path changed.

= 1.3.32 =

* 301 custom redirect loop for option value changed.

= 1.3.31 =

* 301 custom redirect functionality changed.

= 1.3.30 =

* Wp_sfn_sync table condition and sync changed

= 1.3.29 =

* Username and password changes.

= 1.3.28 =

* Auto sync cron integration with Cloudinary image integration.

= 1.3.27 =

* Sale slide Response changes so Added condition for site_code 

= 1.3.26 =

* Changes for Chatmeter, Wells Fargo Link, Synchrony Link shortcodes

= 1.3.25 =
* change for new session script.
* Removed traffic source js.
* Added mmsession js at body end.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`